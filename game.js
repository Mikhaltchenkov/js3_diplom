'use strict';

class Vector {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    plus(vector) {
        if (vector instanceof Vector) {
            return new Vector(this.x + vector.x, this.y + vector.y);
        }
        throw new Error('Ошибка! Смещение должно задаваться объектом типа Vector');
    }

    times(mult) {
        return new Vector(this.x * mult, this.y * mult);
    }
}

class Actor {
    constructor(pos = new Vector(0, 0), size = new Vector(1, 1), speed = new Vector(0, 0)) {
        if (pos instanceof Vector) {
            this.pos = pos;
        } else {
            throw new Error('Ошибка! Позиция должна задаваться объектом типа Vector');
        }
        if (size instanceof Vector) {
            this.size = size;
        } else {
            throw new Error('Ошибка! Размер должен задаваться объектом типа Vector');
        }
        if (speed instanceof Vector) {
            this.speed = speed;
        } else {
            throw new Error('Ошибка! Скорость должна задаваться объектом типа Vector');
        }
    }

    get type() {
        return 'actor';
    }

    get left() {
        return this.pos.x;
    }

    get right() {
        return this.pos.x + this.size.x;
    }

    get top() {
        return this.pos.y;
    }

    get bottom() {
        return this.pos.y + this.size.y;
    }

    //Определение метода act, который ничего не делает
    act() {
    }

    isIntersect(actor) {
        if (actor instanceof Actor) {
            //не пересекаемся сами с собой
            if (actor === this) {
                return false;
            }

            if (this.pos.x == actor.pos.x && this.pos.y == actor.pos.y && (actor.size.x < 0 || actor.size.y < 0)) {
                return false;
            }

            //Объект полностью содержится в нем
            if (this.left < actor.left && this.right > actor.right &&
                this.top < actor.top && this.bottom > actor.bottom) {
                return true;
            }

            //Частичное пересечение
            //верхний левый угол
            if (this.left <= actor.left && this.right > actor.left &&
                this.top <= actor.top && this.bottom > actor.top) {
                return true;
            }

            //Частичное пересечение
            //нижний левый угол
            if (this.left <= actor.left && this.right > actor.left &&
                this.top < actor.bottom && this.bottom >= actor.bottom) {
                return true;
            }

            //верхний правый угол
            if (this.left < actor.right && this.right >= actor.right &&
                this.top <= actor.top && this.bottom > actor.top) {
                return true;
            }

            //нижний правый угол
            if (this.left < actor.right && this.right >= actor.right &&
                this.top < actor.bottom && this.bottom >= actor.bottom) {
                return true;
            }
            return false;
        } else {
            throw new Error('Ошибка! Объект должен быть типа Actor');
        }
    }
}

class Level {
    constructor(grid = [], actors = []) {

        this.grid = grid;

        this.actors = actors;
        this.player = actors.find((el) => el.type == 'player');

        this.status = null;

        this.finishDelay = 1;
    }

    get height() {
        return this.grid.length;
    }

    get width() {
        return this.grid.reduce((max, el) => max > el.length ? max : el.length, 0);
    }

    //Определяем, завершен ли уровень
    isFinished() {
        return this.status != null && this.finishDelay < 0;
    }

    actorAt(actor) {
        if (actor instanceof Actor) {
            if (this.actors.length <= 1) {
                return;
            }
            return this.actors.find((el) => actor.isIntersect(el));
        } else {
            throw new Error('Ошибка! Объект должен быть типа Actor');
        }
    }

    obstacleAt(pos, size) {
        if (pos instanceof Vector && size instanceof Vector) {
            if (pos.x < 0 || pos.x + size.x >= this.width || pos.y < 0) {
                return 'wall';
            }
            if (pos.y + size.y >= this.height) {
                return 'lava';
            }

            for (let y = Math.floor(pos.y); y < Math.ceil(pos.y + size.y); y++) {
                for (let x = Math.floor(pos.x); x < Math.ceil(pos.x + size.x); x++) {
                    if (this.grid[y][x]) {
                        return this.grid[y][x];
                    }
                }
            }

        } else {
            throw new Error('Ошибка! Позиция и размер должны задаваться объектами типа Vector');
        }
    }

    removeActor(actor) {
        let actorIndex = this.actors.indexOf(actor);
        if (actorIndex > -1) {
            this.actors.splice(actorIndex, 1);
        }
    }

    noMoreActors(actorType) {
        if (actorType && typeof actorType == 'string') {
            return this.actors.findIndex((el) => el.type == actorType) == -1;
        } else {
            return this.actors.length == 0;
        }
    }

    playerTouched(type, actor) {
        if (!this.status) {
            if (type == 'lava' || type == 'fireball') {
                this.status = "lost";
            }
            if (type == 'coin') {
                this.removeActor(actor);
                if (this.noMoreActors('coin')) {
                    this.status = 'won';
                }
            }
        }
    }

}

class LevelParser {
    constructor(dictionary = {}) {
        this.dictionary = dictionary;
    }

    actorFromSymbol(symbol) {
        if (symbol && symbol in this.dictionary) {
            return this.dictionary[symbol];
        } else {
            return;
        }
    }

    obstacleFromSymbol(symbol) {
        switch (symbol) {
            case 'x':
                return 'wall';
            case '!':
                return 'lava';
        }
        return;
    }

    createGrid(plan) {
        if (Array.isArray(plan)) {
            return plan.reduce((grid, element) => {
                let gridRow = [];
                for (let index = 0; index < element.length; index++) {
                    gridRow.push(this.obstacleFromSymbol(element[index]));
                }
                grid.push(gridRow);
                return grid;
            }, []);
        } else {
            throw new Error('Ошибка! На входе должен быть массив!')
        }
    }

    createActors(plan) {
        if (Array.isArray(plan)) {
            return plan.reduce((actors, row, y) => {
                for (let x = 0; x < row.length; x++) {
                    let actor = this.actorFromSymbol(row[x]);
                    if (actor && typeof actor === 'function') {
                        let obj = new actor(new Vector(x, y));
                        if (obj instanceof Actor) {
                            actors.push(obj);
                        }
                    }
                }
                return actors;
            }, []);
        } else {
            throw new Error('Ошибка! На входе должен быть массив!');
        }
    }

    parse(plan) {
        let grid = this.createGrid(plan);
        let actors = this.createActors(plan);
        return new Level(grid, actors);
    }
}

class Fireball extends Actor {

    constructor(pos, speed) {
        let size = new Vector(1, 1);
        super(pos, size, speed);
    }

    get type() {
        return 'fireball';
    }

    getNextPosition(time) {
        time = time || 1;
        return this.pos.plus(new Vector(this.speed.x * time, this.speed.y * time));
    }

    handleObstacle() {
        this.speed = this.speed.times(-1);
    }

    act(time, level) {
        let pos = this.getNextPosition(time);
        if (level.obstacleAt(pos, this.size)) {
            this.handleObstacle();
        } else {
            this.pos = pos;
        }
    }
}

class HorizontalFireball extends Fireball {
    constructor(pos) {
        super(pos, new Vector(2, 0));
    }
}

class VerticalFireball extends Fireball {
    constructor(pos) {
        super(pos, new Vector(0, 2));
    }
}

class FireRain extends Fireball {
    constructor(pos) {
        super(pos, new Vector(0, 3));
        this.sourcePos = pos;
    }

    handleObstacle() {
        this.pos = this.sourcePos;
    }
}

class Coin extends Actor {
    constructor(pos = new Vector(0, 0)) {
        pos = pos.plus(new Vector(0.2, 0.1));

        super(pos, new Vector(0.6, 0.6), new Vector(0, 0));
        this.initialPos = pos;

        this.spring = Math.random() * 2 * Math.PI;
    }

    get type() {
        return 'coin';
    }

    get springSpeed() {
        return 8;
    }

    get springDist() {
        return 0.07;
    }

    updateSpring(time) {
        this.spring += this.springSpeed * (time || 1);
    }

    getSpringVector() {
        return new Vector(0, Math.sin(this.spring) * this.springDist);
    }

    getNextPosition(time) {
        this.updateSpring(time);
        return this.initialPos.plus(this.getSpringVector());
    }

    act(time) {
        this.pos = this.getNextPosition(time);
    }

}

class Player extends Actor {
    constructor(pos = new Vector(0, 0)) {
        pos = pos.plus(new Vector(0, -0.5));
        super(pos, new Vector(0.8, 1.5));
    }

    get type() {
        return 'player';
    }
}

const schemas = [
    [
        '                     ',
        '                     ',
        '                     ',
        '                     ',
        '          =          ',
        '                  o  ',
        '  @             xxxx ',
        ' xxxx                ',
        '                     ',
        '        xxxxxx       ',
        '                     ',
        '                     ',
        '                     ',
        '!!!!!!!!!!!!!!!!!!!!!'
    ],
    [
        '                     ',
        '                     ',
        '                     ',
        '                     ',
        '                     ',
        '  xxx              x ',
        '                   x ',
        '                 = x ',
        ' x            o    x ',
        ' x  @       xxxxx  x ',
        ' xxxxx             x ',
        '     x!!!!!!!!!!!!!x ',
        '     xxxxxxxxxxxxxxx ',
        '                     '
    ],
    [
        '                     ',
        '                     ',
        '                     ',
        '                     ',
        ' =                   ',
        '  xxx         o    x ',
        '             xxxx  x ',
        ' o               = x ',
        ' x      xxxx       x ',
        ' x  @              x ',
        ' xxxxx             x ',
        '     x!!!!!!!!!!!!!x ',
        '     xxxxxxxxxxxxxxx ',
        '                     '
    ],
    [
        '                     ',
        '                     ',
        '     @               ',
        ' xxxxxxxxxx          ',
        ' =                   ',
        '              o      ',
        '         xxxxxxxxxx  ',
        '                     ',
        '     o               ',
        ' xxxxxxxxxx          ',
        '                    =',
        '              o      ',
        '         xxxxxxxxxx  ',
        '                     '
    ],
    [
        '                     ',
        '                     ',
        '                     ',
        '                     ',
        '   o       =         ',
        '  xxx  |      o    x ',
        '             xxxx  x ',
        '         o         x ',
        ' x      xxxx       x ',
        ' x  @              x ',
        ' xxxxx             x ',
        '     x!!!!!!!!!!!!!x ',
        '     xxxxxxxxxxxxxxx ',
        '                     '
    ],
    [
        '                     ',
        '                     ',
        '=                    ',
        ' o                   ',
        'xxxx!!!!!xxxx        ',
        '        v            ',
        '                    o',
        '                 xxxx',
        '                     ',
        ' o                   ',
        'xxxxxx     xxxx      ',
        '                     ',
        '                     ',
        ' @               o   ',
        'xxxxxxx!!!xxxxxxxxxxx'
    ],
    [
        '     xxxxxxxxxxx     ',
        '   xx=          xx   ',
        '  x  o         o  x  ',
        ' x   xx       xx   x ',
        ' x  x =x     x =x  x ',
        'x    xx       xx    x',
        'x         @         x',
        'x        xxx        x',
        'x               =   x',
        ' x   o         o   x ',
        ' x   xx!!!!!!!xx   x ',
        '  x    xxxxxxx    x  ',
        '   xx           xx   ',
        '     xxxxxxxxxxx     '
    ],
    [
        '     xxxxxxxxxxx     ',
        '   xx           xx   ',
        '  x  o    =    o  x  ',
        ' x   xx       xx   x ',
        ' x  x =x     x =x  x ',
        'x    xxv      xxv   x',
        'x         @         x',
        'x        xxx        x',
        'x                   x',
        ' x                 x ',
        ' x   o !!!!!!! o   x ',
        '  x  xx       xx  x  ',
        '   xx           xx   ',
        '     xxxxxxxxxxx     '
    ],
    [
        'x        x     v    x',
        'x        x |        x',
        'x @      x        o x',
        'xxxx     x       xxxx',
        'x        x o        x',
        'x      o xxxx       x',
        'x     xxxx          x',
        'x                xxxx',
        'x o                 x',
        'xxxx        =       x',
        'x          xxxx     x',
        'x                   x',
        'x!!!!xxxx!!!!!!!!!!!x',
        'x!!!!!!!!!!!!!!!!!!!x',
    ],
    [
        'v           x        ',
        '       |    x   !!!!!',
        '          o x  vxxxxx',
        '  @      xxxx        ',
        ' xxx                 ',
        '                     ',
        '         =       o   ',
        '    xxx              ',
        '         o     xxxxxx',
        ' o      xxxx         ',
        'xxx                  ',
        '                     ',
        '!!!!!!!!!!!!!!!!!!!!!',
        '                     '
    ],
    [
        '     v              o',
        '  o           o      ',
        ' xxx         xxx     ',
        '       o   =         ',
        '      xxx            ',
        '          |          ',
        ' @          xxxxxxxxx',
        'xxx                 x',
        '                    x',
        '      xxxx      o   x',
        '            x  xxx  x',
        '            x       x',
        '!!!!!!!!!!!!!!!!!!!!!',
        '                     '
    ],
    [
        'x xv x  x x|         ',
        'x x x x x x          ',
        ' x  x x x x          ',
        ' x   x  oxx     o    ',
        '               xxx   ',
        '                     ',
        '       xxxx          ',
        ' @                   ',
        'xxx      ==      o   ',
        '      x o x xxx xx  x',
        '      x x x  x  x x x',
        '      x!x!x  x  x x  ',
        '       x x  xxx x x x',
        '!!!!!!!!!!!!!!!!!!!!!',
    ]
];

const actorDict = {
    '@': Player,
    'v': FireRain,
    '=': HorizontalFireball,
    '|': VerticalFireball,
    'o': Coin
};

const parser = new LevelParser(actorDict);
runGame(schemas, parser, DOMDisplay)
    .then(() => alert('Вы выиграли!'));

